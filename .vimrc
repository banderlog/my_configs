""""" PLUGIN MANAGERS OPTIONS  (needs to precede all other options)
call plug#begin('~/.vim/plugged')

    " color-theme
    Plug 'crusoexia/vim-monokai'

    " CXX syntax
    Plug 'octol/vim-cpp-enhanced-highlight'

    " Asynchronous linting/fixing for Vim
    Plug 'w0rp/ale'

    " repeat maps, not only last action
    Plug 'tpope/vim-repeat'

    " easy change quotes
    Plug 'tpope/vim-surround'

    " easy comments `gcc`
    Plug 'tpope/vim-commentary'

    " improved marks
    Plug 'kshenoy/vim-signature'

    " improved statusbar (needs powerline-fonts)
    Plug 'vim-airline/vim-airline'

    " NERDTree
    Plug 'scrooloose/nerdtree'
    Plug 'Xuyuanp/nerdtree-git-plugin'

    " for fast md tables line-upping
    "   <http://vimcasts.org/episodes/aligning-text-with-tabular-vim/>
    Plug 'godlygeek/tabular'

    " Python
    Plug 'vim-python/python-syntax'

    " Jupyter, `pip install jupytext`
	"   use vim to edit jupyter, discards output cells
    Plug 'goerz/jupytext.vim'
	" Go

	" indent vertical lines
	"Plug 'Yggdroot/indentLine'
	Plug 'lukas-reineke/indent-blankline.nvim'

	" postgresql
	Plug 'lifepillar/pgsql.vim'
call plug#end()


""""" GENERAL OPTIONS
" UTF-8 to all
scriptencoding utf8

" color theme
autocmd Colorscheme * highlight Normal ctermbg=NONE guibg=NONE
colorscheme monokai

" 265 colors for black console
set t_Co=256

" Set linenumbers
set number

" Spaces instead of tabs, 1 tab == 4 spaces
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set shiftround

" Copy indent from current line when starting a new line
set autoindent

" Default vim's file format
set fileformat=unix

" Alway highlight syntax
syntax on

" Detect filetype (FT)
filetype on

" Automatically load plugins for detected FT (ftplugin.vim)
"   it could be global or custom mappings and options for FT
filetype plugin on

" Autoload indent.vim with global/custom indent settings for FT
filetype indent on

" Load ~/.bashrc
set shellcmdflag=-ic

" Enable smart autocompletion ^X^O
"   <http://vim.wikia.com/wiki/Omni_completion>
set omnifunc=syntaxcomplete#Complete

" mkview save/restore options
set viewoptions=cursor,folds,slash,unix

" highlith all search results
set hlsearch

" ^N will look only in current file
set complete-=i

" <https://stackoverflow.com/questions/11489428/how-to-make-vim-paste-from-and-copy-to-systems-clipboard>
set clipboard=unnamedplus

set paste

" prevent hiding characters
set conceallevel=0

" help listchars
set list lcs=tab:>-,trail:-,nbsp:+
set list lcs+=lead:·




""""" FILE TYPES SETTINGS

""" Markdown
" code highlighting in script blocks for selected lanuages
"   for default plugin
let g:markdown_fenced_languages = ['html', 'vim', 'ruby', 'python', 'bash=sh', 'sql', 'r']

" JS
augroup JS
    " prettify code with eslint on `gq` (eslint and nodejs should be installed)
    autocmd FileType javascript setlocal formatprg=prettier-eslint\ --stdin\ --parser\ 'babylon'
    " tab == 2 spaces
    autocmd FileType javascript setlocal shiftwidth=2
    autocmd FileType javascript setlocal tabstop=2
    autocmd FileType javascript setlocal softtabstop=2
augroup END


"""""""""" CUSTOM KEY MAPPINGS
" Buff changing
map <F2> :bp <CR>
map <F3> :bn <CR>

" Open NERDTree
map <C-n> :NERDTreeToggle<CR>

" ALE jump between linter warnings
nmap <silent> <C-k> <Plug>(ale_previous_wrap)
nmap <silent> <C-j> <Plug>(ale_next_wrap)

" select buffer from list
" ^6 toggle
nnoremap <F5> :buffers<CR>:buffer<Space>

"comment toggle as in jupyter notebook
nmap <C-_> gcc


""""" PLUGINS SETTINGS

""" ALE
let g:ale_virtualenv_dir_names = ['venv']

" define linters list
let g:ale_linters = {
\ 'python': ['flake8', 'mypy'],
\ 'cpp': ['cpplint'],
\ 'r': ['lintr'],
\ 'yaml': ['yamllint']
\}
" rust
" <https://github.com/dense-analysis/ale/issues/3350#issuecomment-706020061>
" <https://microblog.desipenguin.com/post/rust-analyzer/>
let g:ale_rust_cargo_use_clippy = executable('cargo-clippy')
let g:ale_rust_analyzer_config = {
      \ 'diagnostics': { 'disabled': ['unresolved-import'] },
      \ 'cargo': { 'loadOutDirsFromCheck': v:true },
      \ 'procMacro': { 'enable': v:true },
      \ 'checkOnSave': { 'command': 'clippy', 'enable': v:true }
      \ }

" do not lint in realtime
let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_insert_leave = 0
let g:ale_lint_on_enter = 1

" redefine error and warning symbols
let g:ale_sign_error = '✘'
let g:ale_sign_warning = '⚠'

" flake8 options
let g:ale_python_flake8_options = '--max-line-length=120 --ignore=E265,E266,E501'
let g:ale_python_mypy_options = '--ignore-missing-imports'
let g:ale_echo_msg_format = '%linter% says %s (%code%)'


""" Airline
" use powerline fonts
let g:airline_powerline_fonts = 1
" use airline's tab line
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#ale#enabled = 1


""" NERDTree
" https://stackoverflow.com/questions/44473097/nerdtree-command-automatically-change-directory-and-root-directory
let g:NERDTreeChDirMode = 2


""" Python syntax
let g:python_highlight_all = 1


""" CXX syntax
let g:cpp_class_scope_highlight = 1
let g:cpp_member_variable_highlight = 1
let g:cpp_class_decl_highlight = 1
let g:cpp_concepts_highlight = 1


""" For Plug 'Yggdroot/indentLine' & Plug 'lukas-reineke/indent-blankline.nvim'
" show preceeding spaces and do not show tabs
let g:indentLine_leadingSpaceEnabled=1
let g:indentLine_fileTypeExclude = ['json', 'markdown', 'dockerfile']
let g:indentLine_char_list = ['|', '¦', '┆', '┊']

""" Plug 'lifepillar/pgsql.vim'
let g:sql_type_default = 'pgsql'

set mouse=a
