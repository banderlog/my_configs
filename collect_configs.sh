#!/bin/bash

FLAG='default'

while getopts "huc" opt; do
  case ${opt} in
    h )
                echo "Usage: cmd [-h] [-u] [-c]"
                echo "  -h      help"
                echo "  -u      push configs from folder to home to update them"
                echo "  -c      collect configs from home to folder"
      ;;
    u ) # push from folder to hoe
                FLAG='upd'
      ;;
    c ) # collect from home to git folder
                FLAG='collect'
      ;;
    * ) # any other option
                FLAG='error'
                echo "Run with -h for help"
      ;;
  esac
done

# Run in ppretend mode if no releveant options were given
if [ $FLAG = 'default' ]; then
   echo "Run with -h for help"
   echo "Now script will check files, but not copy anything"
fi


# SCRIPTS, FONTS, DICTS AND BUILDING CONFIGS ARE NOT INCLUDED
CONFIGS=(
.bashrc
.conkyrc
.inputrc
.tmux.conf
.vimrc
.Xresources
.config/i3/config
.config/i3/i3status.conf
.config/i3/wrapper.py
.config/ranger/rc.conf
.config/ranger/scope.sh
.config/nvim/init.vim
)

for i in "${CONFIGS[@]}"
do
  # check if file not exist
  echo "=================="
  echo "For $i"
  if [[ ! -f ~/$i ]]; then
    echo "~/$i not exist"
    # if it should be within dir
    if [[ "$i" == *"/"* ]]; then
      dirname=${i%/*}
      echo "Extracted $dirname from $i"
      # if dir does not exist
      if [[ ! -d  ~/$dirname ]]; then
        case $FLAG in
          'upd')
            # create dirs
            echo "Creating $dirname"
            mkdir -p ~/$dirname
            ;;
        esac
      fi
    fi
    # copy file from git to HOME
    case $FLAG in
      'upd')
        # update configs in home from git folder
        echo "Copying from git to HOME_DIR: $i"
        cp $i ~/$i
        ;;
    esac
  else
    echo "$i exist"
    # check if it differs
    # 0 means the same also it is boolean true
    if ! cmp -s ~/$i $i; then
      echo "$i has changed"
      case $FLAG in
        'upd')
          # update configs in home from git folder
          echo "Copying from git to HOME_DIR: $i"
          cp $i ~/$i
          ;;
        'collect')
          # copy file into git folder from home
          echo "Copying from HOME_DIR to git: $i"
          cp ~/$i $i
          ;;
      esac
    fi
  fi
done
