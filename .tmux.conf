# To reload config in bash:
# tmux source-file ~/.tmux.conf
# <https://tmuxcheatsheet.com/>

# <https://stackoverflow.com/questions/39368285/can-tmux-save-commands-to-a-file-like-bash-history>
set -g history-file ~/.tmux_history

# <https://www.hamvocke.com/blog/a-guide-to-customizing-your-tmux-conf/>
# <https://webschneider.org/post/italics-in-tmux/>
set -g default-terminal "tmux-256color"

# if run as "tmux attach", create a session if one does not already exist
# https://unix.stackexchange.com/questions/103898/how-to-start-tmux-with-attach-if-a-session-exists
#new-session -n $HOST

# remap prefix from 'C-b' to 'C-a'
# unbind C-b
set-option -g prefix C-a
bind-key C-a send-prefix
bind-key C-a last-window
bind-key Escape copy-mode

# split panes using | and -
# <https://unix.stackexchange.com/a/118381/311674>
bind c new-window -c "#{pane_current_path}"
bind | split-window -h -c "#{pane_current_path}"
bind - split-window -v -c "#{pane_current_path}"
unbind '"'
unbind %

# reload config file (change file location to your the tmux.conf you want to use)
bind r source-file ~/.tmux.conf

set -g base-index 1
setw -g pane-base-index 1

# switch panes using CTRL-arrow without prefix
#bind -n C-Left select-pane -L
#bind -n C-Right select-pane -R
#bind -n C-Up select-pane -U
#bind -n C-Down select-pane -D

# Disable mouse control
# (clickable windows, panes, resizable panes)
set -g mouse off
# Toggle mouse mode (tmux 2.1 and above)
bind-key ` set-option -g mouse

# disable messing-up with clipboard buffer
# tmux CONCATENATES new selections to old without erase
# use SHIFT + mouse to avoid that
set-option -g set-clipboard off

############## begin STATUSBAR ####################
# Status update interval
set -g status-interval 1

# Basic status bar colors
set -g status-style bg=colour234,fg=cyan

# Left side of status bar
set -g status-left-style bg=colour234,fg=cyan
set -g status-left-length 40
set -g status-left "#H #[fg=white]» #[fg=green]#S #[fg=yellow]#I #[fg=cyan]#P"

# Right side of status bar
set -g status-right-style bg=colour234,fg=white
set -g status-right-length 70
# sessionID, windowID, panelID
set -g status-right "#(~/.config/get_gpu_mem_ssd.sh)"
##[fg=white]« #[fg=yellow]%H:%M:%S #[fg=green]%d-%b-%y"

# Window status
# windowID, task_name, window_flags
set -g window-status-format " #I:#W#F "
set -g window-status-current-format " #I:#W#F "

# Current window status
#set -g window-status-current-style bg=red,fg=black
#set -g window-status-current-bg colour238
#set -g window-status-current-fg black

# https://superuser.com/questions/547883/tmux-current-pane-border-not-obvious/
set -g window-style 'fg=colour251, bg=colour234'
set -g window-active-style 'fg=white, bg=black'

# Window with activity status
set -g window-status-activity-style bg=black,fg=yellow

# Window separator
set -g window-status-separator ""

# Window status alignment
set -g status-justify centre

# Pane border
set -g pane-border-style bg=default,fg=default

# Active pane border
set -g pane-active-border-style bg=default,fg=green

# Pane number indicator Ca-q
set -g display-panes-colour white
set -g display-panes-active-colour colour88

# Clock mode
set -g clock-mode-colour red
set -g clock-mode-style 24

# Message
set -g message-style bg=default,fg=default

# Command message
set -g message-command-style bg=default,fg=default

# Mode
set -g mode-style bg=red,fg=default
############## end STATUSBAR ####################


#use vi keys in buffer
setw -g mode-keys vi

# <https://unix.stackexchange.com/questions/348913/copy-selection-to-a-clipboard-in-tmux>
set-option -s set-clipboard off
bind-key -T copy-mode-vi MouseDragEnd1Pane send-keys -X copy-pipe-and-cancel "xclip -se c -i"
#unbind-key -T copy-mode-vi Space
#unbind-key -T copy-mode-vi Enter
#unbind-key -T copy-mode-vi C-v
bind-key -T copy-mode-vi v send-keys -X begin-selection
bind-key -T copy-mode-vi C-v send-keys -X rectangle-toggle
bind-key -T copy-mode-vi y send-keys -X copy-pipe-and-cancel "xclip -sel clip -i"

# <https://superuser.com/questions/325110/how-to-turn-down-the-timeout-between-prefix-key-and-command-key-in-tmux>
# https://superuser.com/questions/1362204/change-the-color-of-tmux-pane-indicators-shown-when-using-display-pane
bind-key -r Up select-pane -U \; display-pane -d 500
bind-key -r Down select-pane -D \; display-pane -d 500
bind-key -r Left select-pane -L \; display-pane -d 500
bind-key -r Right select-pane -R \; display-pane -d 500

# <https://unix.stackexchange.com/questions/14300/moving-tmux-pane-to-window/14301#14301>
# -Nsw will ruin previews (bug?)
# The -h causes it to stack the panes horizontally (with a vertical split)
bind-key @ choose-tree "join-pane -h -t '%%'"   # push active to
bind-key C-@ choose-tree "join-pane -h -s '%%'" # pull here from
# there is a way to move entire window if destination is free:
# 	:move-window [-d] [-s src-window] [-t dst-window]
#   or use shortkey `<prefix>.`  and enter position
# src-window and dst-window have the form `session:window.pane`
# if destion is occupied use different command:
#	:swap-window -s 2 -t 1
set -g focus-events on

#https://askubuntu.com/questions/339546/how-do-i-see-the-history-of-the-commands-i-have-run-in-tmux
set-option -g default-command bash

set -g allow-passthrough on
set -g update-environment "TERM TERM_PROGRAM"
