#!/bin/bash

# create array from xwininfo output (you need to select window)
tmp=(`xwininfo | grep -e "Width: " -e "Height: " -e "Absolute upper-left X:  " -e "Absolute upper-left Y:  " | grep -Po '\d+'`)

WIDTH=${tmp[2]}
HEIGHT=${tmp[3]}
XPOSITION=${tmp[0]}
YPOSITION=${tmp[1]}

echo $WIDTH $XPOSITION
# -f x11grab -- force input format
# -y -- Overwrite output files without asking
# -r 30 -- set frame rate to 30 FPS
# -s -- set frame size
# -i -- input from screen area
# -vcodec -- set video codec
#ffmpeg -f x11grab -y -r 30 -s "$WIDTH"x"$HEIGHT" -i :0.0+"$XPOSITION","$YPOSITION" -vcodec huffyuv out.avi
ffmpeg -f x11grab -y -r 30 -s "$WIDTH"x"$HEIGHT" -i :0.0+"$XPOSITION","$YPOSITION" -c:v libx264 -preset superfast out.mp4

# NEXT DO:
# ffmpeg -i out.avi -ss 00:00:05 -t 00:00:14 -c copy cut.avi
# ffmpeg -i out.avi -ss 00:00:05 -t 00:00:14 cut.mp4
# ffmpeg -i cut.avi cut.mp4
