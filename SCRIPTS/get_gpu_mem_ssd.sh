#!/bin/bash
# Total/Used/Free
#gpu_array=($(nvidia-smi -q -d MEMORY | sed -n '11,13p' | awk '{print $3;}'))
# free RAM
mem_free=$(free -h | sed -n '2p' | awk '{print $7}')
# free SSD
ssd=$(df -h | grep sda1 | awk '{print $4}')
# <https://ru.wikipedia.org/wiki/Load_Average>
# <https://stackoverflow.com/a/11987580/7599215>
load=$(cat /proc/loadavg | awk '{print $1,$2,$3,$4}')
#echo "GPU ${gpu_array[2]}M | MEM ${mem_free} | SSD ${ssd} | ${load}"
echo "MEM ${mem_free} | SSD ${ssd} | ${load}"
