#!/bin/bash
# swith on\off touchpad

# grep touchpads id
t_id=$(xinput | grep "$1" | grep -Po 'id=\K([0-9]*)')
# grep touchpad ststus
t_status=$(xinput --list-props $t_id | grep -Po 'Device Enabled .*:.*\K[0-9]')

# off if on and vice versa
if [ $t_status == 1 ]; then
    xinput --disable $t_id
else
    xinput --enable $t_id
fi
