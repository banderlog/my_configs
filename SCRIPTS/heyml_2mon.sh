#!/bin/bash

xinput --set-prop "Logitech USB Trackball" "Evdev Wheel Emulation" 1
xinput --set-prop "Logitech USB Trackball" "Evdev Wheel Emulation Axes" 6 7 4 5
xinput --set-prop "Logitech USB Trackball" "Evdev Wheel Emulation Button" 3
xinput set-button-map "Logitech USB Trackball" 1 9 3 4 5 6 7 8 2 10 11 12 13

xrandr --newmode  "1368x768"   85.25  1368 1440 1576 1784  768 771 781 798 -hsync +vsync
xrandr --addmode LVDS-1 "1368x768"
xrandr --output LVDS-1 --mode "1368x768"
xrandr --output VGA-1 --mode "1680x1050"
xrandr --output VGA-1 --left-of LVDS-1
