#!/home/bkaba/venv_user/bin/python
from openai import OpenAI
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('--prompt', type=str, required=True, help='DALLE prompt')
args = parser.parse_args()

# https://platform.openai.com/docs/guides/images/usage?context=node
client = OpenAI(api_key='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')

response = client.images.generate(
    model="dall-e-3",
    # model="dall-e-2",
    prompt=args.prompt,
    size="1024x1024",
    quality="standard",
    n=1,
)

image_url = response.data[0].url
print(image_url)
