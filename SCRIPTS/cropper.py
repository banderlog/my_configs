#!/usr/bin/python3
"""
Crop image by interactive selection, save result as separate file.
v 0.3

Select a ROI and then press SPACE or ENTER button
"""
import cv2
import argparse
import sys
import imutils
import numpy as np

ap = argparse.ArgumentParser()
ap.add_argument(dest="image_path", help="path to input image", nargs='+')
args = ap.parse_args()


def main():
    print(args.image_path)
    for i in args.image_path:
        img = cv2.imread(i)
        side = np.argmax(img.shape[:2])
        if side == 0:
            tmp = imutils.resize(img, height=800)
        else:
            tmp = imutils.resize(img, width=800)

        (x, y, w, h) = cv2.selectROI(i, tmp, fromCenter=False, showCrosshair=True)
        new_y, new_x = tmp.shape[:2]
        old_y, old_x = img.shape[:2]

        y = int(y / new_y * old_y)
        h = int(h / new_y * old_y)
        x = int(x / new_x * old_x)
        w = int(w / new_x * old_x)

        crop = img[y:y + h, x:x + w]
        if len(crop) > 0:
            cv2.imwrite(i + '_crop', crop)


if __name__ == '__main__':
    sys.exit(main() or 0)
