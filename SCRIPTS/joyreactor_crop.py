""" Crop joyreactor pics
crop 15 pixels from bottom
"""
import cv2
import sys

if len(sys.argv) == 1:
    print('USAGE: joyreactor_crop.py [img1 img2 .. imgN]')

for pic_name in sys.argv[1:]:
    tmp = cv2.imread(pic_name)
    cv2.imwrite(pic_name + '_', tmp[0:-15, :])
