#!/bin/bash

CURRENT=$(i3-msg -t get_workspaces | jq '.[] | select(.focused==true).name' | cut -d"\"" -f2)
NEW=$("title"| dmenu -i -p "New title:")
i3-msg "rename workspace $CURRENT to $CURRENT:$NEW"
