#!/usr/bin/python3
""" Convert mp3 tags to utf8

v.0.1.0
`for i in ./*; do decode_mp3tags_to_utf8.py "$i"; done`
Kabakov Borys -- 2019
"""

import os
import mutagen.id3
import argparse
import sys

# https://stackoverflow.com/questions/15753701/argparse-option-for-passing-a-list-as-option
ap = argparse.ArgumentParser()
ap.add_argument(dest="mp3_path", help="path to input image")
# will change default `--enc en1 enc2 enc3`
ap.add_argument('--enc', dest="try_encodings", help="encoding to try",
                nargs='+', type=str, default=['cp1251'])
# will not change default, need to put `-e` each time
#ap.add_argument('-e', dest="try_encodings", action='append',
#                type=str, default=['cp1251'])
args = ap.parse_args()


def get_mp3_files(path: str) -> list:
    "Just get all mpe files in dir, if it is a dir"
    if os.path.isdir(path):
        p = path
        tmp = os.listdir(p)
        tmp = [os.path.join(p, i) for i in tmp if i.lower().endswith('.mp3')]
        tmp.sort
    elif os.path.isfile(path) and path.lower().endswith('.mp3'):
        tmp = [path]
    else:
        print("Give me a MP3!")
        sys.exit(0)
    return tmp


def try_convert(byte_str: str, try_encodings: list) -> str:
    """ Try all encodeings, accept first without Exception
    """
    result = None
    for encoding in try_encodings:
        try:
            result = byte_str.decode(encoding)
        except UnicodeError:
            print("FUCK: ", encoding)
            pass
        else:
            return result


def is_field_relevant(value):
    "if it is has text in LATIN1 encoding and it is a string"
    if (value.encoding == 0) and hasattr(value, 'text'):
        if all(isinstance(t, str) for t in value.text):
            return True


def main():
    print(args.try_encodings)
    mp3_files = get_mp3_files(args.mp3_path)
    for path in mp3_files:
        id3 = mutagen.id3.ID3(path)
        # loop over all metainfo
        for key, value in id3.items():
            if is_field_relevant(value):
                # get bytes
                byte_str = '\n'.join(value.text).encode('iso-8859-1')
                # loop over supposed encodings
                utf8_str = try_convert(byte_str, args.try_encodings)
                if not utf8_str:
                    print('Failed for {} key {}'.format(path, key))
                else:
                    value.text = utf8_str.split('\n')
                    # 3 for utf8
                    value.encoding = 3
        id3.save(path)
        print("[OK]  ", path)


if __name__ == '__main__':
    sys.exit(main() or 0)
