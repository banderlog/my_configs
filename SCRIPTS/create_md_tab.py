""" Get txt from clipboard
    change "A\t\t\tB\t\tC\n" to "|A|B|C|\n"
    add "|---|" as second row
    put to buffer
"""
import re
import pyperclip

txt = pyperclip.paste().split('\n')
md = []

for i, line in enumerate(txt):
    tmp = '|' + re.sub(r'\t+', '|', line) + '|\n'
    md.append(tmp)
    if i == 0:
        columns = len(tmp.split('|')) - 2
        tmp = ('|---' * columns) + '|\n'
        md.append(tmp)

pyperclip.copy(''.join(md))
