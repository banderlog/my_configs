#!/bin/bash
# <https://github.com/naelstrof/slop>
slop=$(slop -k -o -f "%x %y %w %h %g %i") || exit 1
read -r X Y W H G ID < <(echo $slop)

ffmpeg -f x11grab -y -r 30 -s "$W"x"$H" -i :0.0+"$X","$Y" -vf pad="width=ceil(iw/2)*2:height=ceil(ih/2)*2" -c:v libx264 -preset superfast -pix_fmt yuv420p out.mp4
