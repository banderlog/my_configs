#!/usr/bin/python3
""" In case you used wrong keymap to print message
    It works with clipboard buffer only (^c)

    v 0.2
"""
import pyperclip
import argparse
import sys

ap = argparse.ArgumentParser()
ap.add_argument("--conv", dest="conv", help="select from to conversion",
                choices=['en_ru', 'ru_en'], default='en_ru', type=str)
args = ap.parse_args()

# create dictionaries for ru,en keymaps
en = "{}{}{}{}".format("qwertyuiop[]asdfghjkl;'zxcvbnm,./",
                       "QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>?",
                       "`", '~@#$%^&|')
ru = "{}{}{}{}".format("йцукенгшщзхъфывапролджэячсмитьбю.",
                       "ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ,",
                       "ё", 'Ё"№;%:?/')
en_ru = {k: v for k, v in zip(en, ru)}
ru_en = {k: v for k, v in zip(ru, en)}


def convert(d: dict, txt: str) -> str:
    'universal keymap converter'
    converted = []
    keys = d.keys()
    for i in txt:
        if i in keys:
            i = d[i]
        converted.append(i)
    return "".join(converted)


def main():
    # get from buffer
    txt = pyperclip.paste()
    if args.conv == 'en_ru':
        answer = convert(en_ru, txt)
    else:
        answer = convert(ru_en, txt)
    # put to buffer
    pyperclip.copy(answer)


if __name__ == '__main__':
    sys.exit(main() or 0)
