# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth
# append to the history file, don't overwrite it
shopt -s histappend
# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# set a fancy prompt (non-color, unless we know we "want" color)
#case "$TERM" in
#    xterm-color|*-256color) export PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ ';;
#esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
fi


export TERM=st-256color
export PATH=~/.local/bin:~/venv/bin:/usr/sbin:/usr/local/bin:$PATH
export PS1=$(if [[ $RANGER_LEVEL ]]; then echo "[ranger]"; fi)$PS1
export EDITOR=nvim


# https://bugs.launchpad.net/ubuntu/+source/bsdmainutils/+bug/908233
alias cal='ncal -bM'
alias ip='ip -c'
alias vim='nvim'

ffmpeg_mp4() {
	# convert any to h264/mp4 no h:w ratio problems and Telegram compatible
	# https://stackoverflow.com/a/38703079/7599215
    ffmpeg -i "$1" -an -c:v libx264 -vf pad="width=ceil(iw/2)*2:height=ceil(ih/2)*2" -pix_fmt yuv420p "${1%.*}_tg.mp4"
}

# docker
# export DOCKER_HOST=unix:///run/user/1000/docker.sock
# https://stackoverflow.com/a/51897942/7599215
# who sets that???
# unset ${!DOCKER_*}

# https://unix.stackexchange.com/questions/73498/how-to-cycle-through-reverse-i-search-in-bash
stty -ixon

#weather: curl https://wttr.in/
#cheatsh: curl https://cheat.sh/
#bench_speed: curl https://bench.sh/ | bash

myip_info() {
myip=$(curl -s https://ipv4.icanhazip.com/); echo $myip; curl -s http://ip-api.com/json/$myip | jq
}
. "$HOME/.cargo/env"
